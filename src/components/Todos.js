import { useEffect, useState } from "react";
import AddTodoForm from "./AddTodoForm";
import Todo from "./Todo";

const Todos = (props) => {

    const [todos, setTodos] = useState([{ id: 0, title: "Titel", description: "Description", done: "False" }])
    const { user } = props;


    useEffect(() => {

        const fetchTodos = async () => {

            let response = await fetch(`${process.env.REACT_APP_BASE_URL}/api/user/${user.id}/todo`, {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${user.token}`
                }
            });
            let todos = await response.json();


            setTodos(todos)
        }

        fetchTodos();


    }, [])

    return (
        <div>
            {todos.map(todo => <Todo user={user} key={todo.id} todo={todo} setTodos={setTodos} />)}
            <AddTodoForm user={user} setTodos={setTodos} />
        </div>
    )

}

export default Todos;