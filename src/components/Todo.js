
const Todo = (props) => {

    const { todo, setTodos, user } = props;

    const handleDelete = async (id) => {

        await fetch(`${process.env.REACT_APP_BASE_URL}/api/todo/${id}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${user.token}`
            }
        })

        let response = await fetch(`${process.env.REACT_APP_BASE_URL}/api/todo`, {
            headers: {
                Authorization: `Bearer ${user.token}`
            }
        })
        let todos = await response.json()

        console.log("todos", todos)

        setTodos(todos)

    }

    return (
        <div>
            <h2>{todo.title}</h2>
            <p>{todo.description}</p>
            <p>Avklarad: <input
                type="checkbox"
                checked={todo.done}
                onChange={() => console.log("To be implemented...")}
            />
            </p>
            <button onClick={() => handleDelete(todo.id)}>Ta bort todo</button>
        </div>
    )
}

export default Todo;